﻿using UnityEngine;
using System.Collections;

public class Bat : MonoBehaviour {

	// Alue, jonka sisällä mailan liikuttaminen voidaan aloittaa
	// ja jonka sisällä maila voi liikkua
	public GameObject PlayerArea;

	int inputType = 0;			// Määritelty käytettävän laitteen mukaan, 1 = kosketuslaite, 2 = tietokone
	// Molemmille pelaajille omat muuttujat
	bool[] 		dragStarted = false;	// Mailan liike käynnissä
	Vector2[] 	batInit;				// Mailan sijainti liikkeen alkaessa (tai edellinen sijainti?)
	Vector2[] 	dragInit;				// Kosketuskohta raahauksen alkaessa
	Vector2[] 	dragCurrent;			// Nykyinen kosketuskohta
	Vector2[] 	dragDelta;				// Siirtymä alkuperäisestä sijainnista

	public GameObject TouchMarker;

	// Use this for initialization
	void Start () {

		if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer) 
			inputType = 1;
		else if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor) 
			inputType = 2;	 
	}
	
	// Update is called exactly 50 times per second
	void FixedUpdate () {

		if (inputType == 1) 
			HandleTouchInput();
			/*
			if (Input.GetMouseButton (0))
				HandleInput ();
			else dragStarted = false;
			*/
		else if (inputType == 2)
		{
			HandleMouseInput();
			/*
			int nbTouches = Input.touchCount;
			if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
				HandleInput ();
			else dragStarted = false;
			*/
		}
	}

	void HandleTouchInput()
	{
		// only one touch per each player matters when moving the bat (int redFingerId, blueFingerId)
		// 

		int nbTouches = Input.touchCount;
		if(nbTouches > 0) 
		{
			// käy läpi touchit
			for (int i = 0; i < nbTouches; i++) 
			{
				// tsekkaa onko jomman kumman pelaajan alueella ja mailan sisällä, mutta fingerID ei määräävä sormi
				Touch touch = Input.GetTouch(i);
					// jos niin lisää pelaajan kosketusarrayhyn
				// jos määräävän fingerin kosketus loppui juuri
					// käy läpi array
					// valitse ensimmäinen uudeksi määrääväksi sormeksi
					// alusta batInit ja dragInit uusiksi

			
				
			}
			// jos bat törmää seinään, laske dragInit ja batInit uusiks sen mukaan, kuin bat voi liikkua, vaikka sormi menisi kentän laidan yli ja batin ulkopuolelle


			if (Input.GetTouch(0).phase == TouchPhase.Moved)
			{
				if (!dragStarted) {
					dragStarted = true;
					
					dragInit = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
					RaycastHit2D hit = Physics2D.Raycast(dragInit, Vector2.zero);
					if (hit.collider != null)
					{
						if (hit.collider == this.GetComponent<Collider2D>()) 
						{
							batInit = this.transform.position;
							//Debug.Log(batInit);
							TouchMarker.transform.position = new Vector3(dragInit.x, dragInit.y, -0.55f);
						}
					}
				}
				else
				{
					dragCurrent = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
					dragDelta = dragCurrent - dragInit;
					//Debug.Log(dragDelta);
					this.transform.position = batInit + dragDelta;
				}
				
			}
			else
			{
				dragStarted = false;
			}
		}
	}

	void HandleMouseInput()
	{
		if (Input.GetMouseButton (0)) {			
			if (!dragStarted) {


				dragInit = Camera.main.ScreenToWorldPoint(Input.mousePosition);
				RaycastHit2D hit = Physics2D.Raycast(dragInit, Vector2.zero);
				if (hit)
				{
					if (hit.collider == this.GetComponent<Collider2D>()) 
					{
						dragStarted = true;
						//Debug.Log(hit.collider.name);
						batInit = this.transform.position;
						//Debug.Log(batInit);

						TouchMarker.transform.position = new Vector3(dragInit.x, dragInit.y, -0.55f);
					}
				}
			}
			else
			{
				dragCurrent = Camera.main.ScreenToWorldPoint(Input.mousePosition);
				dragDelta = dragCurrent - dragInit;
				//Debug.Log(dragDelta);
				this.transform.position = batInit + dragDelta;
			}

		}
		else
		{
			dragStarted = false;
		}
	}

	// To get 
	Vector2 GetInputPosition()
	{
		// Using computer and mouse
		if (inputType == 1)
			return Input.mousePosition;
		// Using touch device
		else if (inputType == 2)
			return Input.GetTouch (0).position;
		// Error case, shouldn't happen
		else {
			Debug.Log ("Input device not set");
			return new Vector2 (0, 0);
		}
	}

	void HandleInput()
	{
		if (!dragStarted) {
			
			dragInit = Camera.main.ScreenToWorldPoint(GetInputPosition());
			RaycastHit2D hit = Physics2D.Raycast(dragInit, Vector2.zero);
			if (hit)
			{
				Debug.Log(hit.collider.name);
				Debug.Log(hit.collider);
				Debug.Log(this.GetComponent<Collider2D>());
				if (hit.collider == this.GetComponent<Collider2D>()) 
				{
					dragStarted = true;
					batInit = this.transform.position;
					//Debug.Log(batInit);
					
					TouchMarker.transform.position = new Vector3(dragInit.x, dragInit.y, -0.55f);
				}
			}
		}
		else
		{
			dragCurrent = Camera.main.ScreenToWorldPoint(GetInputPosition());
			dragDelta = dragCurrent - dragInit;
			//Debug.Log(dragDelta);
			this.transform.position = batInit + dragDelta;
		}
	}

}